package com.tougher.mobs.v2.lidle.data;

import org.bukkit.entity.EntityType;

/**
 * This enum class contains a list of the supported mobs by the plugin. As of Minecraft 1.11 ALL
 * hostile mobs are supported.
 * */
public enum MobValues {

	BLAZE(EntityType.BLAZE),
	CAVE_SPIDER(EntityType.CAVE_SPIDER),
	CREEPER(EntityType.CREEPER),
	ELDER_GUARDIAN(EntityType.ELDER_GUARDIAN),
	ENDERMAN(EntityType.ENDERMAN),
	ENERMITE(EntityType.ENDERMITE),
	EVOKER(EntityType.EVOKER),
	GHAST(EntityType.GHAST),
	GUARDIAN(EntityType.GUARDIAN),
	HUSK(EntityType.HUSK),
	MAGMA_CUBE(EntityType.MAGMA_CUBE),
	PIG_ZOMBIE(EntityType.PIG_ZOMBIE),
	POLAR_BEAR(EntityType.POLAR_BEAR),
	SHULKER(EntityType.SHULKER),
	SILVERFISH(EntityType.SILVERFISH),
	SKELETON(EntityType.SKELETON),
	SLIME(EntityType.SLIME),
	SPIDER(EntityType.SPIDER),
	STRAY(EntityType.STRAY),
	VEX(EntityType.VEX),
	VINDICATOR(EntityType.VINDICATOR),
	WITCH(EntityType.WITCH),
	ZOMBIE(EntityType.ZOMBIE);
	
	private EntityType entityType;
	
	MobValues(EntityType entityType){
		this.entityType = entityType;
	}
	
	public EntityType getEntityType(){
		return this.entityType;
	}
	
}
