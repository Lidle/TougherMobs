package com.tougher.mobs.v2.lidle.fun;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Bat;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.event.Listener;

import com.tougher.mobs.v2.lidle.main.TougherMobs;


public class LootBat implements Listener{

	TougherMobs plugin;
	
	private static final String LOOT_BAT_NAME = ChatColor.GOLD + "" + ChatColor.BOLD + "Loot Bat";
	
	private ItemStack batHelmet = createBatItem(Material.DIAMOND_HELMET, ChatColor.RED + "Bat Helmet", Enchantment.PROTECTION_ENVIRONMENTAL, 5),
					  batChestplate = createBatItem(Material.DIAMOND_CHESTPLATE, ChatColor.RED + "Bat Chestplate", Enchantment.PROTECTION_ENVIRONMENTAL, 5),
					  batLeggings = createBatItem(Material.DIAMOND_LEGGINGS, ChatColor.RED + "Bat Leggings", Enchantment.PROTECTION_ENVIRONMENTAL, 5),
					  batBoots = createBatItem(Material.DIAMOND_BOOTS, ChatColor.RED + "Bat Boots", Enchantment.PROTECTION_ENVIRONMENTAL, 5);
	private ItemStack[] batGear = {batHelmet, batChestplate, batLeggings, batBoots};
	
	public LootBat(TougherMobs plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.plugin = plugin;
	}
	
	@EventHandler
	public void batSpawnEvent(EntitySpawnEvent e){
		if(e.getEntity() instanceof Bat){
			Random rand = new Random();
			
			if(rand.nextInt(100) + 1 <= 1){
				e.getEntity().setCustomName(LOOT_BAT_NAME);
				e.getEntity().setGlowing(true);
			}
		}
	}

	@EventHandler
	public void batDeathEvent(EntityDeathEvent e){
		if(e.getEntity() instanceof Bat){
			if(e.getEntity().getCustomName() == null)
				return;
			
			String name = e.getEntity().getCustomName();
			if(name.equalsIgnoreCase(LOOT_BAT_NAME)){
				Bat bat = (Bat) e.getEntity();
				Random rand = new Random();
				int slot = rand.nextInt(4) + 0;
				e.getEntity().getWorld().dropItemNaturally(bat.getLocation(), batGear[slot]);
			}
		}
	}
	
	private ItemStack createBatItem(Material type, String name, Enchantment enchant, int enchantLevel){
		ItemStack item = new ItemStack(type);
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(name);
		item.setItemMeta(im);
		item.addUnsafeEnchantment(enchant, enchantLevel);
		return item;
	}
}
