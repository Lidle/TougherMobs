package com.tougher.mobs.v2.lidle.players;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerUtils {

	//Collection of armor player is wearing
	public static List<Material> playerArmor = new ArrayList<Material>();
	public static List<ItemStack> armor = new ArrayList<ItemStack>();
	
	private static void getArmor(Player p){
		Material helm,
				  chest,
				  legs,
				  boots;
		
		playerArmor.clear();
		armor.clear();
		
		if(p.getInventory().getHelmet() != null){
			helm = p.getInventory().getHelmet().getType();
			armor.add(p.getInventory().getHelmet());
			playerArmor.add(helm);
		}
		
		if(p.getInventory().getChestplate() != null){
			chest = p.getInventory().getChestplate().getType();
			armor.add(p.getInventory().getChestplate());
			playerArmor.add(chest);
		}
		
		if(p.getInventory().getLeggings() != null){
			legs = p.getInventory().getLeggings().getType();
			armor.add(p.getInventory().getLeggings());
			playerArmor.add(legs);
		}
		
		if(p.getInventory().getBoots() != null){
			boots = p.getInventory().getBoots().getType();
			armor.add(p.getInventory().getBoots());
			playerArmor.add(boots);
		}
	}
	
	public static int getArmorRating(Player p){
		getArmor(p);
		
		int armorRating = 0;
		
		for(ArmorRatings rating : ArmorRatings.values()){
			for(int i = 0; i < playerArmor.size(); i++){
				if(playerArmor.get(i) == rating.getMaterial()){
					armorRating += rating.getArmorRating();
				}
			}
		}
		
		return armorRating;
	}
	
	public static int healthMultiplier(int armorRating){
		
		int healthMultiplier = 0;
		
		if(armorRating > 14 && armorRating <= 60){
			//1 heart
			healthMultiplier = 2;
			return healthMultiplier;
		}
		
		else if(armorRating > 60 && armorRating <= 68){
			//3 hearts
			healthMultiplier = 6;
			return healthMultiplier;
		}
		
		else if (armorRating > 68 && armorRating <= 76){
			//4 hearts
			healthMultiplier = 8;
			return healthMultiplier;
		}
		
		else if (armorRating > 76 && armorRating <= 84){
			//6 hearts
			healthMultiplier = 12;
			return healthMultiplier;
		}
		
		else if (armorRating > 84 && armorRating <= 100){
			//10 hearts
			healthMultiplier = 20;
			return healthMultiplier;
		}
		
		else if (armorRating > 100){
			healthMultiplier = 30;
			return healthMultiplier;
		}
		
		return healthMultiplier;
	}
	
	public static int dmgMultiplier(int armorRating){
		
		int dmgMultiplier = 0;
		
		if(armorRating > 14 && armorRating <= 60){
			//1 heart
			dmgMultiplier = 2;
			return dmgMultiplier;
		}
		
		else if(armorRating > 60 && armorRating <= 68){
			//1.5 hearts
			dmgMultiplier = 3;
			return dmgMultiplier;
		}
		
		else if (armorRating > 68 && armorRating <= 76){
			//2 hearts
			dmgMultiplier = 4;
			return dmgMultiplier;
		}
		
		else if (armorRating > 76 && armorRating <= 84){
			//2.5 hearts
			dmgMultiplier = 5;
			return dmgMultiplier;
		}
		
		else if (armorRating > 84 && armorRating <= 100){
			//4 hearts
			dmgMultiplier = 8;
			return dmgMultiplier;
		}
		
		else if (armorRating > 100){
			dmgMultiplier = 14;
			return dmgMultiplier;
		}
		
		return dmgMultiplier;
		
	}

	public static int getEnchantmentRating(Player p){
		int enchantRating = 0;
		ItemStack item = null;
		
		if(p.getInventory().getItemInMainHand() != null)
			item = p.getInventory().getItemInMainHand();
		
		for(int i = 0; i < armor.size(); i++){
			ItemStack armorPiece = armor.get(i);
			
			for(Enchantment ench : Enchantment.values()){
				if(armorPiece.containsEnchantment(ench)){
					enchantRating += armorPiece.getEnchantmentLevel(ench);
				}
				
				if(item.containsEnchantment(ench))
					enchantRating += item.getEnchantmentLevel(ench);
			}
			
		}
		
		return enchantRating;
	}
	
	public static int getWeaponRating(Player p){
		int weaponRating = 0;
		Material mat = p.getInventory().getItemInMainHand().getType();
		
		for(WeaponRatings wr : WeaponRatings.values()){
			if(wr.getMaterial() == mat){
				weaponRating += wr.getWeaponRating();
				return weaponRating;
			}
		}
		
		return weaponRating;
	}
	
	public static int getGearRating(Player p){
		int gearRating = 0;
		gearRating += getArmorRating(p) + getEnchantmentRating(p) + getWeaponRating(p);
		return gearRating;
	}
}
