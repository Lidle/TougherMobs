package com.tougher.mobs.v2.lidle.players;

import java.util.List;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class GlobalDamageEvents implements Listener {

	TougherMobs plugin;
	
	public GlobalDamageEvents(TougherMobs plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.plugin = plugin;
	}
	
	//Player shoots an arrow at an entity
	@EventHandler
	public void entityDamageByEntity(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Arrow){
			Arrow arrow = (Arrow) e.getDamager();
			
			if(arrow.getShooter() instanceof Player){
				
				if(e.getEntity() instanceof LivingEntity && !(e.getEntity() instanceof Player)){
					//Returns if the entity is a boss
					if(MobUtils.isBoss((LivingEntity) e.getEntity()))
						return;
					
					MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) arrow.getShooter());
					return;
				}
				
			}
			
		}
	}
	
	//Player splashes potion at an entity
	@EventHandler
	public void potionSplashEvent(PotionSplashEvent e){
		//Checking to find the nearest player from potion impact
		List<Entity> nearbyEntities = e.getEntity().getNearbyEntities(8, 3, 8);
		Player p = null;
		for(int i = 0; i < nearbyEntities.size(); i++){
			if(nearbyEntities.get(i) instanceof Player){
				p = (Player) nearbyEntities.get(i);
			}
		}
		
		if(p != null){
			List<LivingEntity> entities = (List<LivingEntity>) e.getAffectedEntities();
			for(int i = 0; i < entities.size(); i++){
				if(!(entities.get(i) instanceof Player)){
					MobUtils.setMaxHealth((LivingEntity) entities.get(i), p);
				}
			}
		}
	}
	
//	@EventHandler
//	public void onEntityDamageEvent(EntityDamageByEntityEvent e){
//		//Player damages Boss
//		if(e.getDamager() instanceof Player
//				&& e.getEntity() instanceof Monster){
//
//			if(new Mob().isBoss(e.getEntity().getCustomName())){
//				e.setDamage(e.getDamage() / 2);
//			}
//		}
//		
//		//Boss damages player
//		else if (e.getDamager() instanceof Monster
//					&& e.getEntity() instanceof Player){
//			int gearRating = PlayerUtils.getArmorRating((Player) e.getEntity());
//			int dmgMultiplier = PlayerUtils.dmgMultiplier(gearRating);
//			
//			if(new Mob().isBoss(e.getDamager().getCustomName())){
//				dmgMultiplier += dmgMultiplier;
//				e.setDamage(dmgMultiplier);
//			}
//		}
//		
//		//Boss shoots projectiles
//		else if(e.getDamager() instanceof Fireball
//					&& e.getEntity() instanceof Player){
//			
//			Fireball fb = (Fireball) e.getDamager();
//			if(fb.getShooter() instanceof Monster){
//				
//				int gearRating = PlayerUtils.getArmorRating((Player) e.getEntity());
//				int dmgMultiplier = PlayerUtils.dmgMultiplier(gearRating);
//				
//				Entity entity = (Entity) fb.getShooter();
//				if(new Mob().isBoss(entity.getCustomName())){
//					dmgMultiplier += dmgMultiplier;
//					e.setDamage(dmgMultiplier);
//				}
//				
//			}
//			
//		}
//	}
	
}
