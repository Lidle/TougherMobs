package com.tougher.mobs.v2.lidle.players;

import org.bukkit.Material;

public enum ArmorRatings {

	//Armor Rating = 100
	DIAMOND_HELMET(Material.DIAMOND_HELMET, 25),
	DIAMOND_CHESTPLATE(Material.DIAMOND_CHESTPLATE, 25),
	DIAMOND_LEGGINGS(Material.DIAMOND_LEGGINGS, 25),
	DIAMOND_BOOTS(Material.DIAMOND_BOOTS, 25),
	
	//Armor Rating = 84
	IRON_HELMET(Material.IRON_HELMET, 20),
	IRON_CHESTPLATE(Material.IRON_CHESTPLATE, 22),
	IRON_LEGGINGS(Material.IRON_LEGGINGS, 22),
	IRON_BOOTS(Material.IRON_BOOTS, 20),
	
	//Armor Rating 76
	GOLD_HELMET(Material.GOLD_HELMET, 18),
	GOLD_CHESTPLATE(Material.GOLD_CHESTPLATE, 20),
	GOLD_LEGGINGS(Material.GOLD_LEGGINGS, 20),
	GOLD_BOOTS(Material.GOLD_BOOTS, 18),
	
	//Armor Rating 68
	CHAINMAIL_HELMET(Material.CHAINMAIL_HELMET, 16),
	CHAINMAIL_CHESTPLATE(Material.CHAINMAIL_CHESTPLATE, 18),
	CHAINMAIL_LEGGINGS(Material.CHAINMAIL_LEGGINGS, 18),
	CHAINMAIL_BOOTS(Material.CHAINMAIL_BOOTS, 16),
	
	//Armor Rating 60
	LEATHER_HELMET(Material.LEATHER_HELMET, 14),
	LEATHER_CHESTPLATE(Material.LEATHER_CHESTPLATE, 16),
	LEATHER_LEGGINGS(Material.LEATHER_LEGGINGS, 16),
	LEATHER_BOOTS(Material.LEATHER_BOOTS, 14);
	
	private Material material;
	private int armorRating;
	
	ArmorRatings(Material material, int armorValue){
		this.material = material;
		this.armorRating = armorValue;
	}

	protected Material getMaterial() {
		return material;
	}

	protected int getArmorRating() {
		return armorRating;
	}
	
}
