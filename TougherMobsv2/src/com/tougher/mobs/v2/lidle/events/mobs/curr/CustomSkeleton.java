package com.tougher.mobs.v2.lidle.events.mobs.curr;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class CustomSkeleton extends CustomMob{

	public static boolean customMoves = true;
	
	public CustomSkeleton() {
		super(EntityType.SKELETON);
	}
	
	@Override
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e) {
		if(playerDamagedEntity(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getEntity(), (Player) e.getDamager());
		}
	}

	@Override
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e) {
		if(isArrowFromSkeleton(e) && isSupportedWorld(e.getEntity().getWorld())){
			Arrow arrow = (Arrow) e.getDamager();
			LivingEntity le = (LivingEntity) arrow.getShooter();
			scaleMobHealthToPlayer(le, (Player) e.getDamager());
			e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
		}
	}
	
	private boolean isArrowFromSkeleton(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Arrow){
			Arrow a = (Arrow) e.getDamager();
			if(a.getShooter() instanceof Skeleton)
				return true;
			return false;
		}

		return false;
	}

}
