package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.Random;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class SkeletonEvent implements Listener, TougherMobEvent {

	public static boolean custom_moves = true;
	
	TougherMobs pl;

	public SkeletonEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}

	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Skeleton){
				//Player damages skeleton
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}

	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(isArrowFromSkeleton(e)){
			if(e.getEntity() instanceof Player){
				Arrow a = (Arrow) e.getDamager();
				//Skeleton arrow damages player
				if(MobUtils.isBoss((LivingEntity) a.getShooter()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) a.getShooter(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				setLevitation((LivingEntity) e.getEntity(), 60);
				return;
			}
		}
	}


	//Checks to see if the arrow shot is from a skeleton
	//Pre-Condition: EntityDamageByEntity event is passed as a parameter
	//Post-Condition: Method extrapolates data from the event to see if arrow
	//was shot from a skeleton
	private boolean isArrowFromSkeleton(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Arrow){
			Arrow a = (Arrow) e.getDamager();
			if(a.getShooter() instanceof Skeleton)
				return true;
			return false;
		}

		return false;
	}
	
	private void setLevitation(LivingEntity e, int duration){
		if(!custom_moves)
			return;
		
		Random rand = new Random();
		
		if (rand.nextInt(100) + 0 <= 15) {
			PotionEffect potionEffect = new PotionEffect(PotionEffectType.LEVITATION, duration, 1);
			e.addPotionEffect(potionEffect);
		}
	}
}
