package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class EndermanEvent implements Listener, TougherMobEvent {

	public static List<Integer> blackListedEndermanBlocks = new ArrayList<Integer>();
	public static boolean custom_moves = true;

	TougherMobs pl;

	public EndermanEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}

	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Enderman){
				//Player damages enderman
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}

	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Enderman){
			if(e.getEntity() instanceof Player){
				//Enderman damages player
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				destroyBlocks(e);
				return;
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void destroyBlocks(EntityDamageByEntityEvent e){
		if(!custom_moves)
			return;

		Random rand = new Random();

		//25% chance to run
		if (rand.nextInt(100) + 0 <= 25) {
			boolean continue_ = true;
			Player p = (Player) e.getEntity();
			Location loc = p.getLocation();
			loc.setY(loc.getY() - 1);
			
			Material mat = p.getWorld().getBlockAt(loc).getType();
			if (mat == Material.AIR) {
				continue_ = false;
			}
			
			if (blackListedEndermanBlocks.contains(mat.getId()))
				continue_ = false;
			
			if (continue_) {
				p.getWorld().getBlockAt(loc).setType(Material.AIR);
				p.getWorld().dropItem(loc, new ItemStack(mat));
			} 
			
		}
	}

}
