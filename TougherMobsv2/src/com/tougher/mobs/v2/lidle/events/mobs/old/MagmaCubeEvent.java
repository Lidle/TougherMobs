package com.tougher.mobs.v2.lidle.events.mobs.old;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class MagmaCubeEvent implements Listener, TougherMobEvent {

	TougherMobs pl;
	
	public MagmaCubeEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof MagmaCube){
				//Player damages magmacube
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof MagmaCube){
			if(e.getEntity() instanceof Player){
				//Magma cube damages player
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				return;
			}
		}
	}
	
}
