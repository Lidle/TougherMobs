package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class BlazeEvent implements Listener, TougherMobEvent{

	public static boolean custom_moves = true;
	
	TougherMobs pl;
	
	public BlazeEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		//If it is not a supported world
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Blaze){
				
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When the blaze fire ball hits the player
	@EventHandler
	public void fireBallDamagePlayer(EntityDamageByEntityEvent e){
		//If it is not a supported world
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
				
		if(isFireballFromBlaze(e)){
			if(e.getEntity() instanceof Player){
				Fireball fb = (Fireball) e.getDamager();
				//The fireball shooter is the blaze in this case. Has already been checked
				//(LivingEntity) fb.getShooter() returns the Blaze.
				
				if(MobUtils.isBoss((LivingEntity) fb.getShooter()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) fb.getShooter(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				runAttacks((Player) e.getEntity());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Blaze){
			if(e.getEntity() instanceof Player){
				//Blaze Damages player
				
				//We do not want to scale down bosses
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(MobUtils.getDamage((Player) e.getEntity()));
				runAttacks((Player) e.getEntity());
				return;
			}
		}
	}
	
	//Checks to see if the fireball shot is from a Blaze
	//Pre-Condition: EntityDamageByEntity event is passed as a parameter
	//Post-Condition: Method extrapolates data from the event to see if fireball
	//was shot from a Blaze
	private boolean isFireballFromBlaze(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Fireball){
			Fireball fb = (Fireball) e.getDamager();
			if(fb.getShooter() instanceof Blaze)
				return true;
			return false;
		}
		
		return false;
	}
	
	private void lavaAttack(Player p){
		Location loc = p.getLocation();
		
		Random rand = new Random();
		
		if(rand.nextInt(100) + 1 <= 5){
			loc.getWorld().getBlockAt(loc).setType(Material.LAVA);
			return;
		}
	}
	
	private void fireAttack(Player p){
		Random rand = new Random();
		
		if(rand.nextInt(100) + 1 <= 20){
			List<Entity> list = p.getNearbyEntities(2, 2, 2);

			for(int i = 0; i < list.size(); i++){
				if(list.get(i) instanceof Player){
					list.get(i).setFireTicks(60);
				}
			}
		}
		
	}
	
	private void runAttacks(Player p){
		if(!custom_moves)
			return;
		
		lavaAttack(p);
		fireAttack(p);
	}
	
}
