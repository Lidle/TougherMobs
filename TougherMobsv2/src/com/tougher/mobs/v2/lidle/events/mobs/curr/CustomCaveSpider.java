package com.tougher.mobs.v2.lidle.events.mobs.curr;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class CustomCaveSpider extends CustomMob{

	public static boolean customMoves = true;
	
	public CustomCaveSpider() {
		super(EntityType.CAVE_SPIDER);
		customAttacks.add(CustomAttacks.Attacks.WEB);
	}

	@Override
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e) {
		if(playerDamagedEntity(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getEntity(), (Player) e.getDamager());
		}
	}

	@Override
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e) {
		if(entityDamagedPlayer(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getDamager(), (Player) e.getEntity());
			e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
		}
	}
	
}
