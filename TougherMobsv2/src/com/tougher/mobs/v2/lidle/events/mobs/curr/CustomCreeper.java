package com.tougher.mobs.v2.lidle.events.mobs.curr;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class CustomCreeper extends CustomMob implements Listener{

	public static List<Integer> blacklistedCreeperBlocks = new ArrayList<Integer>();
	public static boolean customMoves = true;
	
	public CustomCreeper() {
		super(EntityType.CREEPER);
		TougherMobs.pl.getServer().getPluginManager().registerEvents(this, TougherMobs.pl);
	}
	
	@Override
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e) {
		if(playerDamagedEntity(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getEntity(), (Player) e.getDamager());
		}
	}

	@Override
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e) {
		if(entityDamagedPlayer(e)){
			//There is no MobUtils.setMaxHealth() because creepers only explode.
			//Setting a max health while exploding could potentially cause some glitches.
			e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
		}
	}

	@EventHandler
	public void onCreeperExplodeEvent(EntityExplodeEvent e){
		//Checking to see if it was a creeper that exploded
		if(isSupportedEntity(e.getEntity())){
			if(!customMoves) return;
			
			if(TougherMobs.rand.nextInt(5) + 1 <= 1){
				createFireOnExplosion(e);
			}
		}
	}
	
	//Replaces SOME blocks that were subject to being exploded with fire
	@SuppressWarnings("deprecation")
	private void createFireOnExplosion(EntityExplodeEvent e){
		List<Block> blockList = e.blockList();
		
		e.setCancelled(true);
		for (int i = 0; i < blockList.size(); i++) {
			if (TougherMobs.rand.nextInt(4) + 1 == 1) {
				Block block = blockList.get(i);
				Location loc = block.getLocation();		
				
				loc.setX(loc.getX() + 1);
				
				if(!(blacklistedCreeperBlocks.contains(block.getTypeId()))){
					loc.getWorld().getBlockAt(loc).setType(Material.FIRE, true);
				}
				
			}
		}
	}
}
