package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class CreeperEvent implements Listener, TougherMobEvent {

	public static List<Integer> blacklistedCreeperBlocks = new ArrayList<Integer>();
	public static boolean custom_moves = true;
	
	TougherMobs pl;
	
	public CreeperEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Creeper){
				//Player damages creeper
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Creeper){
			if(e.getEntity() instanceof Player){
				//Creeper damages player
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				//There is no MobUtils.setMaxHealth() because creepers only explode.
				//Setting a max health while exploding could potentially cause some glitches.
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				return;
			}
		}
	}
	
	//Creeper explosion event
	@EventHandler
	public void creeperExplosionEvent(EntityExplodeEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if (custom_moves) {
			Random rand = new Random();
			
			//1 in 5 chance of Creeper causing a fire
			if (e.getEntity() instanceof Creeper
					&& rand.nextInt(5) + 1 == 3) {
				createFireOnExplosion(e);
			} 
		}
	}
	
	@SuppressWarnings("deprecation")
	private void createFireOnExplosion(EntityExplodeEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		List<Block> blockList = e.blockList();
		Random rand = new Random();
		
		for (int i = 0; i < blockList.size(); i++) {
			if (rand.nextInt(4) + 1 == 1) {
				Block block = blockList.get(i);
				Location loc = block.getLocation();
				e.setCancelled(true);
				loc.setX(loc.getX() + 1);
				
				if(!(blacklistedCreeperBlocks.contains(block.getTypeId())))
					loc.getWorld().getBlockAt(loc).setType(Material.FIRE, true);
			}
		}
	}
	
}
