package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.Random;

import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.PolarBear;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class PolarBearEvent  implements Listener, TougherMobEvent {

	public static boolean custom_moves = true;
	
	TougherMobs pl;
	
	public PolarBearEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof PolarBear){
				//Player damages polar bear
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof PolarBear){
			if(e.getEntity() instanceof Player){
				//Polar bear damages player
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				freezePlayer((Player) e.getEntity());
				return;
			}
		}
	}
	
	private void freezePlayer(Player p){
		if(!custom_moves)
			return;
		
		Random rand = new Random();
		
		if(rand.nextInt(100) + 1 <= 20){
			
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 80, 1));
			p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 1));
			p.getWorld().spawnParticle(Particle.SPELL, p.getLocation(), 10);
			p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_ATTACK_STRONG, 1.0F, 1.0F);
		}
		
	}
}
