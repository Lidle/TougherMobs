package com.tougher.mobs.v2.lidle.events.custom_bosses;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.ItemStack;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.main.command.YMLOperations;

/**
 * Handles all the events that include custom bosses.
 * */
public class CustomBossEvent implements Listener {

	TougherMobs pl;

	private File file = new File("plugins//TougherMobs//Bosses//");

	public CustomBossEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}

	//Event to initialize the boss when they spawn
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onCustomBossSpawn(EntitySpawnEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return;

		if(!(e.getEntity() instanceof LivingEntity)) return;

		//This stores all of the bosses with the same entity type that is trying to be spawned
		List<String> potentialBosses = getPotentialBosses(e.getEntityType());

		if(potentialBosses == null) return;

		Random rand = new Random();

		//3% Chance of spawning
		if (rand.nextInt(100) + 1 <= 3) {
			//Picks a random boss
			if (potentialBosses.size() > 0) {
				//Cast to living entity so health can be set
				LivingEntity le = (LivingEntity) e.getEntity();
				//This random # picks a random supported boss
				int index = rand.nextInt(potentialBosses.size()) + 1;
				String bossName = potentialBosses.get(index - 1);

				if(YMLOperations.ymlGet("health", bossName) == null) return;

				int health = (int) YMLOperations.ymlGet("health", bossName);

				if(health == 0) return;

				//Gets the boss name
				le.setCustomName(bossName);
				//Set health
				le.setMaxHealth(health);
				le.setHealth(health);
			} 
		}

	}

	//Boss damages a player
	//Damager = Boss
	//Entity = Player
	@EventHandler
	public void onCustomBossDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return;

		//Damager has to be a Boss and NOT a Player
		if(!(e.getDamager() instanceof LivingEntity) && !(e.getDamager() instanceof Player)) return;

		//Entity does not have a custom name meaning it is not a boss
		if(e.getDamager().getCustomName() == null) return;

		//This stores all of the bosses with the same entity type that is executed in the event
		List<String> potentialBosses = getPotentialBosses(e.getDamager().getType());

		if(potentialBosses == null) return;

		String bossName = e.getDamager().getCustomName();

		for(int i = 0; i < potentialBosses.size(); i++){
			if(bossName.equalsIgnoreCase(potentialBosses.get(i))){
				if(YMLOperations.ymlGet("damage", bossName) == null) return; 

				int dmg = (int) YMLOperations.ymlGet("damage", bossName);

				if(dmg >= 0)
					e.setDamage(dmg);
			}
		}
	}

	@EventHandler
	public void onCustomBossDamagePlayerByArrow(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return;

		//Damager is an Arrow
		if(e.getDamager() instanceof Arrow){
			Arrow a = (Arrow) e.getDamager();
			//Must be a mob and not a player
			if(a.getShooter() instanceof LivingEntity && !(a.getShooter() instanceof Player)){
				LivingEntity le = (LivingEntity) a.getShooter();

				//Entity does not have a custom name meaning it is not a boss
				if(le.getCustomName() == null) return;

				//This stores all of the bosses with the same entity type that is executed in the event
				List<String> potentialBosses = getPotentialBosses(le.getType());

				if(potentialBosses == null) return;

				String bossName = le.getCustomName();

				for(int i = 0; i < potentialBosses.size(); i++){
					if(bossName.equalsIgnoreCase(potentialBosses.get(i))){
						if(YMLOperations.ymlGet("damage", bossName) == null) return; 

						int dmg = (int) YMLOperations.ymlGet("damage", bossName);

						if(dmg >= 0)
							e.setDamage(dmg);
					}
				}
			}
		}
	}

	//Event to drop custom boss items
	@EventHandler
	public void onCustomBossDeathDrop(EntityDeathEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return;

		if(!(e.getEntity() instanceof LivingEntity)) return;

		if(e.getEntity().getCustomName() == null) return;


		//Has a custom name
		List<String> potentialBosses = getPotentialBosses(e.getEntityType());
		String bossName = e.getEntity().getCustomName();

		//Mob has no name
		if(potentialBosses == null) return;

		//Code to retrieve an item from the bosses loot table and drop it on the ground
		for(int i = 0; i < potentialBosses.size(); i++){
			if(bossName.equals(potentialBosses.get(i))){
				List<ItemStack> bossLoot = getBossLootTable(bossName);

				if(bossLoot != null){
					Random rand = new Random();
					int index = rand.nextInt(bossLoot.size()) + 1;
					e.getDrops().clear();
					e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), bossLoot.get(index - 1));
				}

				return;
			}
		}


	}

	//Event to drop custom boss xp
	@EventHandler
	public void onCustomBossDeathXP(EntityDeathEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return;

		if(!(e.getEntity() instanceof LivingEntity)) return;

		//If entity dies by fire (i.e. Sunlight)
		if(e.getEntity().getLastDamageCause().getCause().equals(DamageCause.FIRE_TICK)) return;

		if(e.getEntity().getCustomName() == null) return;

		String bossName = e.getEntity().getCustomName();
		List<String> potentialBosses = getPotentialBosses(e.getEntityType());

		//Mob has no name
		if(potentialBosses == null) return;

		for(int i = 0; i < potentialBosses.size(); i++){
			//If they are the same name
			if(bossName.equalsIgnoreCase(potentialBosses.get(i))){
				e.setDroppedExp(getBossXP(bossName));
				return;
			}

		}


	}

	//Retrieves all saved <boss_name>.yml files
	private File[] getYMLFiles(){
		return file.listFiles(new FilenameFilter(){
			public boolean accept(File dir, String name){
				return name.toLowerCase().endsWith(".yml");
			}
		});
	}

	//Loads all the bosses with matching entity types from the saved .yml files and returns their name
	private List<String> getPotentialBosses(EntityType type){
		List<String> potentialBosses = new ArrayList<String>();
		//Loads all the saved custom boss .yml files for reference
		File[] ymlFiles = getYMLFiles();

		if(ymlFiles == null)
			return null;

		//Iterates through the .yml files to see which types match the spawning entity type
		for(int i = 0; i < ymlFiles.length; i++){
			YamlConfiguration yml = YamlConfiguration.loadConfiguration(ymlFiles[i]);
			String tmp = (String) yml.get("type");

			if(type.toString().equalsIgnoreCase(tmp)){
				potentialBosses.add(yml.getString("name"));
			}
		}

		return potentialBosses;
	}

	@SuppressWarnings("unchecked")
	private List<ItemStack> getBossLootTable(String bossName){
		List<ItemStack> lootTable = new ArrayList<ItemStack>();
		//Loads all the saved custom boss .yml files for reference
		File file = new File("plugins//TougherMobs//Bosses//" + bossName + ".yml");

		YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
		lootTable = (List<ItemStack>) yml.getList("drops");

		if(lootTable == null)
			return null;
		else
			return lootTable;
	}

	private int getBossXP(String bossName){
		File file = new File("plugins//TougherMobs//Bosses//" + bossName + ".yml");
		YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);

		int xp = yml.getInt("xp");

		return xp;
	}
}
