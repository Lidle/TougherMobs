package com.tougher.mobs.v2.lidle.events.mobs.curr;

import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * This houses all <b>GENERAL</b> attacks that can be used by
 * almost any mob.  Any attack that is unique to the mob itself <i>(i.e. Creepers)</i>
 * are housed in their class-file.
 * */
public class CustomAttacks {

	public enum Attacks {
		
		FIRE("fire"), //Sets player on fire
		FREEZE("freeze"), //Applies slowness to the player
		LAVA("lava"), //Puts player in a block of lava
		WEB("web");
		
		private String attackType;
		
		Attacks(String attackType){
			this.attackType = attackType;
		}
		
		public String getAttackType(){
			return this.attackType;
		}
	}
	
	@SuppressWarnings("unused")
	private static Random rand = new Random();
	
	private static void fireAttack(Player p){
		List<Entity> nearbyEntities = p.getNearbyEntities(2, 2, 2);
		
		for(Entity e : nearbyEntities){
			if(e instanceof Player)
				e.setFireTicks(60);
		}
	}
	
	private static void freezeAttack(Player p){
		p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 80, 1));
		p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 1));
		p.getWorld().spawnParticle(Particle.SPELL, p.getLocation(), 10);
		p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_ATTACK_STRONG, 1.0F, 1.0F);
	}
	
	private static void lavaAttack(Player p){
		Location loc = p.getLocation();
		loc.getWorld().getBlockAt(loc).setType(Material.LAVA);
	}
	
	private static void webAttack(Player p){
		if(p.getWorld().getBlockAt(p.getLocation()).getType() == Material.AIR){
			p.getWorld().getBlockAt(p.getLocation()).setType(Material.WEB);
		}
	}
	
	public static void executeCustomAttack(String attackType, Player p){
		
		switch(attackType){
			case "fire":
				fireAttack(p);
				break;
			case "freeze":
				freezeAttack(p);
				break;
			case "lava":
				lavaAttack(p);
				break;
			case "web":
				webAttack(p);
				break;
		}
	}
}
