package com.tougher.mobs.v2.lidle.events.bosses;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import com.tougher.mobs.v2.lidle.data.MobValues;
import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

/**
 * Responsible for handling all events and modifications regarding all the boss mobs.
 * */
public class BossEvent implements Listener{

	TougherMobs pl;

	public BossEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}

	//Boss spawn event
	@EventHandler
	public void bossSpawnEvent(EntitySpawnEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		EntityType et = e.getEntityType();
		Random rand = new Random();
		
		if (rand.nextInt(100) + 0 <= 1) {
			for (MobValues mv : MobValues.values()) {
				//If it is a supported mob, make it a boss
				if (mv.getEntityType().equals(et)) {
					createBoss((LivingEntity) e.getEntity());
					return;
				}
			} 
		}
	}
	
	@EventHandler
	public void bossDeathEvent(EntityDeathEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(MobUtils.bossNames.contains(e.getEntity().getUniqueId().toString())){
			e.setDroppedExp(e.getDroppedExp() * 10);
			int index = MobUtils.bossNames.indexOf(e.getEntity().getUniqueId().toString());
			MobUtils.bossNames.remove(index);
			return;
		}
		
	}

	//Melee events
	@EventHandler
	public void playerDamagesBossEvent(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity){
			LivingEntity le = (LivingEntity) e.getEntity();

			//If it is a boss, we divide the damage by 2
			if(MobUtils.isBoss(le)){
				e.setDamage(e.getDamage() / 2);
				return;
			}
		}

	}

	@EventHandler
	public void bossDamagesPlayerEvent(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof LivingEntity && e.getEntity() instanceof Player){
			LivingEntity le = (LivingEntity) e.getDamager();
			Player p = (Player) e.getEntity();

			//If player isnt blocking, double damage.
			//Otherwise, do normal damage
			if(MobUtils.isBoss(le)){
				if(p.isBlocking()){
					e.setDamage(e.getDamage());
					return;
				}
				else{
					e.setDamage(e.getDamage() * 2);
					return;
				}
			}
		}
	}

	//Range Event
	@EventHandler
	public void playerShootsBossEvent(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Arrow){
			Arrow a = (Arrow) e.getDamager();
			
			if(a.getShooter() instanceof Player){
				if (e.getEntity() instanceof LivingEntity && !(e.getEntity() instanceof Player)){
					if(MobUtils.isBoss((LivingEntity)e.getEntity())){
						e.setDamage(e.getDamage() / 2);
						return;
					}
				}
			}
			
		}
	}
	
	@EventHandler
	public void bossShootsPlayerEvent(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Arrow){
			Arrow a = (Arrow) e.getDamager();
			
			if(a.getShooter() instanceof LivingEntity){
				if (e.getEntity() instanceof Player){
					Player p = (Player) e.getEntity();
					if(MobUtils.isBoss((LivingEntity)a.getShooter())){
						
						if(!(p.isBlocking())){
							e.setDamage(e.getDamage() * 2);
							return;
						}
						
					}
				}
			}
			
		}
	}

	//Potion event
	@EventHandler
	public void playerSplashesBoss(PotionSplashEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getAffectedEntities().equals(null))
			return;
		
		Collection<LivingEntity> affectedEntities = e.getAffectedEntities();
		Iterator<LivingEntity> it = (Iterator<LivingEntity>) affectedEntities.iterator();
		
		while(it.hasNext()){
			LivingEntity le = (LivingEntity) it.next();
			//Unaffected by potions
			if(MobUtils.isBoss(le)){
				e.setIntensity(le, 0);
			}
		}
		
		return;
	}
	
	@EventHandler
	public void bossSplashesPlayer(PotionSplashEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getAffectedEntities().equals(null))
			return;
		
		Collection<LivingEntity> affectedEntities = e.getAffectedEntities();
		Iterator<LivingEntity> it = (Iterator<LivingEntity>) affectedEntities.iterator();
		
		if(!(MobUtils.isBoss((LivingEntity)e.getEntity().getShooter())))
			return;
		
		while(it.hasNext()){
			LivingEntity le = (LivingEntity) it.next();
			//+1 to the potions intensity
			if(le instanceof Player){
				e.setIntensity(le, e.getIntensity(le) + 1);
			}
		}
		
		return;
	}
	
	@SuppressWarnings("deprecation")
	private void createBoss(LivingEntity e){
		String bossName = getNameFromType(e.getType());

		e.setCustomName(bossName);
		e.setMaxHealth(200);
		e.setHealth(200);


		MobUtils.bossNames.add(e.getUniqueId().toString());
	}

	private String getNameFromType(EntityType type){
		String name = type.toString();
		String substr1 = name.substring(0, 1).toUpperCase();
		String substr2 = name.substring(1).toLowerCase();
		String bossName = substr1 + substr2;

		//Removes underscores from boss names
		if(bossName.contains("_")){
			int index = bossName.indexOf('_');
			bossName = bossName.replace('_', ' ');
			String char1 = bossName.substring(index + 1, index + 2).toUpperCase();
			char tmp = char1.charAt(0);
			bossName = bossName.replace(bossName.charAt(index + 1), tmp);	
		}

		bossName = ChatColor.RED + "" + ChatColor.BOLD + bossName + " Boss";


		return bossName;
	}
}
