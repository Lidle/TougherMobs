package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class SpiderEvent implements Listener, TougherMobEvent {

	public static boolean custom_moves = true;
	
	TougherMobs pl;
	Random rand;
	
	public SpiderEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//Spider spawn event
	@EventHandler
	public void spiderSpawnEvent(CreatureSpawnEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getEntity() instanceof Spider){
			rand = new Random();
			
			if(rand.nextInt(100) + 0 <= 10){
				e.getEntity().addPotionEffect(getInvisibleEffect());
				return;
			}
		}
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Spider){
				//Player damages spider
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Spider){
			if(e.getEntity() instanceof Player){
				//Spider damages player
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				runMoves((Player) e.getEntity());
				return;
			}
		}
	}
	
	private PotionEffect getPoisonEffect(){
		PotionEffect poisonPotion = new PotionEffect(PotionEffectType.POISON, 100, 1);
		return poisonPotion;
	}
	
	private PotionEffect getInvisibleEffect(){
		PotionEffect invisiblePotion = new PotionEffect(PotionEffectType.INVISIBILITY, 2147000000, 1);
		return invisiblePotion;
	}
	
	private void setEntityOnFire(LivingEntity e){
		if(e instanceof Player)
			e.setFireTicks(3);		
	}
	
	private void createWeb(Player p){
		if(p.getWorld().getBlockAt(p.getLocation()).getType() == Material.AIR)
			p.getWorld().getBlockAt(p.getLocation()).setType(Material.WEB);
		
	}
	
	private void runMoves(Player p){
		if(!custom_moves)
			return;
		
		Random rand = new Random();

		if (rand.nextInt(20) + 0 == 10)
			p.addPotionEffect(getPoisonEffect());
		
		if (rand.nextInt(20) + 0 == 11)
			setEntityOnFire(p);
		
		if (rand.nextInt(20) + 0 == 12)
			createWeb(p);
	
	}
	
}
