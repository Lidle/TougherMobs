package com.tougher.mobs.v2.lidle.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

import com.tougher.mobs.v2.lidle.enchants.armor.ArmorEnchants;
import com.tougher.mobs.v2.lidle.enchants.bow.BowEnchants;
import com.tougher.mobs.v2.lidle.enchants.weapons.WeaponEnchants;
import com.tougher.mobs.v2.lidle.players.PlayerUtils;


/**
 * This class is responsible for every operationg involving making a mob tougher.
 * Included in here, we have armor/weapon generation, setting health and a list of
 * mobs/bosses that have been modified by the plugin already.
 * */
public class MobUtils {

	public static List<String> mobUUID = new ArrayList<String>();
	public static List<String> bossNames = new ArrayList<String>();
	public static boolean bossEnabled = true;
	
	protected static ItemStack helmet,
	chest,
	legs,
	boots;

	protected static ItemStack mainHand,
	offHand;

	protected static int tier5 = 75,   //25% Chance for leather/wood
			tier4 = 50,   	   //20% Chance for chain/stone
			tier3 = 30,   	   //15% Chance for gold 
			tier2 = 15,   	   //10% CHance for iron 
			tier1 = 5;  	       //5% Chance for diamond 



	protected ItemStack getHelmet() {
		return helmet;
	}



	protected static void setHelmet(ItemStack helmet) {
		MobUtils.helmet = helmet;
	}



	protected ItemStack getChest() {
		return chest;
	}



	protected static void setChest(ItemStack chest) {
		MobUtils.chest = chest;
	}



	protected ItemStack getLegs() {
		return legs;
	}



	protected static void setLegs(ItemStack legs) {
		MobUtils.legs = legs;
	}



	protected ItemStack getBoots() {
		return boots;
	}



	protected static void setBoots(ItemStack boots) {
		MobUtils.boots = boots;
	}



	protected static ItemStack getMainHand() {
		return mainHand;
	}



	protected static void setMainHand(ItemStack mainHand) {
		MobUtils.mainHand = mainHand;
	}



	protected static ItemStack getOffHand() {
		return offHand;
	}



	protected static void setOffHand(ItemStack offHand) {
		MobUtils.offHand = offHand;
	}

	protected static void setEntityArmor(LivingEntity e){
		if(helmet != null)
			e.getEquipment().setHelmet(helmet);

		if(chest != null)
			e.getEquipment().setChestplate(chest);

		if(legs != null)
			e.getEquipment().setLeggings(legs);

		if(boots != null)
			e.getEquipment().setBoots(boots);
	}

	protected static void setEntityWeapons(LivingEntity e){
		if(mainHand != null)
			e.getEquipment().setItemInMainHand(mainHand);

		if(offHand != null)
			e.getEquipment().setItemInOffHand(offHand);
	}

	protected static void clearEntityArmor(){
		setHelmet(null);
		setChest(null);
		setLegs(null);
		setBoots(null);
	}

	protected static void clearEntityWeapons(){
		setMainHand(null);
		setOffHand(null);
	}

	//The chance of what kind of armor will spawn (i.e. Helmet, chest, legs or boots)
	protected static void armorChance(ItemStack helm, ItemStack chest, ItemStack legs, ItemStack boots){
		Random rand = new Random();
		int armorChance = rand.nextInt(100) + 0;
		int enchantChance = rand.nextInt(100) + 0;

		//Equip a chest
		if(armorChance <= 25){

			if(enchantChance <= 25){
				chest = randomArmorEnchant(chest);
			}

			setChest(chest);
		}

		//Equip legs
		else if(armorChance <= 50){	

			if(enchantChance <= 25){
				legs = randomArmorEnchant(legs);
			}

			setLegs(legs);
		}

		//Equip boots
		else if(armorChance <= 75){

			if(enchantChance <= 25){
				boots = randomArmorEnchant(boots);
			}

			setBoots(boots);
		}

		//Equip helmet
		else if(armorChance <= 100){

			if(enchantChance <= 25){
				helmet = randomArmorEnchant(helm);
			}

			setHelmet(helm);
		}
	}

	protected static void weaponChance(ItemStack mainHand, ItemStack offHand){
		Random rand = new Random();
		int weaponChance = rand.nextInt(100) + 0;
		int enchantChance = rand.nextInt(100) + 0;

		//Equips both main hand and off hand
		if(weaponChance <= 15){
			if(enchantChance <= 25){
					if(mainHand.getType() == Material.BOW){
						mainHand = randomBowEnchant(mainHand);
					}

					else{
						mainHand = randomWeaponEnchant(mainHand);
					}

				
			}

			setMainHand(mainHand);
			setOffHand(offHand);
		}

		//Equips just main hand
		else if(weaponChance <= 35){
			if(enchantChance <= 25){
					//Apply a random enchant to the bow
					if(mainHand.getType() == Material.BOW){
						mainHand = randomBowEnchant(mainHand);
					}

					//Apply a random enchant to spawned weapon
					else{
						mainHand = randomWeaponEnchant(mainHand);
					}	
			}
			setMainHand(mainHand);
		}
	}

	public static void runGenerateArmor(LivingEntity e){
		Random rand = new Random();
		int runtime = rand.nextInt(4) + 0;

		clearEntityArmor();	

		for (int i = 0; i < runtime; i++) {

			int chance = rand.nextInt(100) + 0;

			if (chance <= tier1) {
				ItemStack helm = new ItemStack(Material.DIAMOND_HELMET),
						chest = new ItemStack(Material.DIAMOND_CHESTPLATE),
						legs = new ItemStack(Material.DIAMOND_LEGGINGS), 
						boots = new ItemStack(Material.DIAMOND_BOOTS);
				armorChance(helm, chest, legs, boots);
				setEntityArmor(e);
			}

			else if (chance <= tier2) {
				ItemStack helm = new ItemStack(Material.IRON_HELMET), 
						chest = new ItemStack(Material.IRON_CHESTPLATE),
						legs = new ItemStack(Material.IRON_LEGGINGS), 
						boots = new ItemStack(Material.IRON_BOOTS);
				armorChance(helm, chest, legs, boots);
				setEntityArmor(e);
			}

			else if (chance <= tier3) {
				ItemStack helm = new ItemStack(Material.GOLD_HELMET), 
						chest = new ItemStack(Material.GOLD_CHESTPLATE),
						legs = new ItemStack(Material.GOLD_LEGGINGS), 
						boots = new ItemStack(Material.GOLD_BOOTS);
				armorChance(helm, chest, legs, boots);
				setEntityArmor(e);
			}

			else if (chance <= tier4) {
				ItemStack helm = new ItemStack(Material.CHAINMAIL_HELMET),
						chest = new ItemStack(Material.CHAINMAIL_CHESTPLATE),
						legs = new ItemStack(Material.CHAINMAIL_LEGGINGS),
						boots = new ItemStack(Material.CHAINMAIL_BOOTS);
				armorChance(helm, chest, legs, boots);
				setEntityArmor(e);
			}

			else if (chance <= tier5) {
				ItemStack helm = new ItemStack(Material.LEATHER_HELMET),
						chest = new ItemStack(Material.LEATHER_CHESTPLATE),
						legs = new ItemStack(Material.LEATHER_LEGGINGS), 
						boots = new ItemStack(Material.LEATHER_BOOTS);
				armorChance(helm, chest, legs, boots);
				setEntityArmor(e);
			} 
		}
	}

	@SuppressWarnings("deprecation")
	public static void runGenerateWeapons(LivingEntity e){
		Random rand = new Random();
		
		if(e instanceof Skeleton && e.getType().getTypeId() != 51){
			int chance = rand.nextInt(100) + 0;
			
			if(chance <= 50){
				ItemStack mainHand = new ItemStack(Material.BOW),
						offHand = new ItemStack(Material.SHIELD);
				weaponChance(mainHand, offHand);
				setEntityWeapons(e);
			}
		}
		
		else if(e instanceof Zombie){
			randomWeapons(e);
		}
	}

	private static void randomWeapons(LivingEntity e){
		Random rand = new Random();
		int chance = rand.nextInt(100) + 0;
		
		if (chance <= tier1) {
			ItemStack mainHand = new ItemStack(Material.DIAMOND_SWORD),
					offHand = new ItemStack(Material.SHIELD);
			weaponChance(mainHand, offHand);
			setEntityWeapons(e);
		}

		else if (chance <= tier2) {
			ItemStack mainHand = new ItemStack(Material.IRON_SWORD),
					offHand = new ItemStack(Material.SHIELD);
			weaponChance(mainHand, offHand);
			setEntityWeapons(e);
		}

		else if (chance <= tier3) {
			ItemStack mainHand = new ItemStack(Material.GOLD_SWORD),
					offHand = new ItemStack(Material.SHIELD);

			weaponChance(mainHand, offHand);
			setEntityWeapons(e);
		}

		else if (chance <= tier4) {
			ItemStack mainHand = new ItemStack(Material.STONE_SWORD),
					offHand = new ItemStack(Material.SHIELD);

			weaponChance(mainHand, offHand);
			setEntityWeapons(e);
		}

		else if (chance <= tier5) {
			ItemStack mainHand = new ItemStack(Material.WOOD_SWORD),
					offHand = new ItemStack(Material.SHIELD);

			weaponChance(mainHand, offHand);
			setEntityWeapons(e);
		} 
	}
	
	public static ItemStack randomArmorEnchant(ItemStack armor){

		Random rand = new Random();
		int enchantNumber = rand.nextInt(ArmorEnchants.SIZE) + 1;
		int loop = 1;


		for(ArmorEnchants enchant : ArmorEnchants.values()){
			if(loop == enchantNumber){

				int level = rand.nextInt(enchant.getMaxLevel()) + 1;
				if (enchant.getEnchantment().canEnchantItem(armor) && enchant != null) {
					armor.addEnchantment(enchant.getEnchantment(), level);

					return armor;
				}

			}

			loop++;
		}

		return null;
	}

	public static ItemStack randomWeaponEnchant(ItemStack weapon){

		Random rand = new Random();
		int enchantNumber = rand.nextInt(WeaponEnchants.SIZE) + 1;
		int loop = 1;
		int level;


		for(WeaponEnchants enchant : WeaponEnchants.values()){
			if(loop == enchantNumber){
				
				level = rand.nextInt(enchant.getMaxLevel()) + enchant.getEnchantment().getStartLevel();

				if (enchant.getEnchantment().canEnchantItem(weapon)) {
					weapon.addEnchantment(enchant.getEnchantment(), level);
					return weapon;
				} 

			}

			loop++;
		}

		return null;
	}

	public static ItemStack randomBowEnchant(ItemStack bow){

		Random rand = new Random();
		int level;
		int enchantNumber = rand.nextInt(BowEnchants.SIZE) + 1;
		int loop = 1;


		for(BowEnchants enchant : BowEnchants.values()){
			if(loop == enchantNumber){

				level = rand.nextInt(enchant.getMaxLevel()) + enchant.getEnchantment().getStartLevel();
				
				if (enchant.getEnchantment().canEnchantItem(bow)) {
					bow.addEnchantment(enchant.getEnchantment(), level);
					return bow;
				} 

			}

			loop++;
		}

		return null;
	}
	
	public static void removeEntity(LivingEntity entity){
		if(mobUUID.contains(entity.getUniqueId().toString()))
			mobUUID.remove(entity.getUniqueId().toString());
	}

	
	@SuppressWarnings("deprecation")
	public static void setMaxHealth(LivingEntity e, Player p){
		if (!(mobUUID.contains(e.getUniqueId().toString()))) {
			//Gets the rating of the players gear
			int gearRating = PlayerUtils.getGearRating(p);
			//Assigns it a health multiplier depending on how powerful it is
			int healthMultiplier = PlayerUtils.healthMultiplier(gearRating);
			int health = (int) e.getMaxHealth() + healthMultiplier;
			e.setMaxHealth(health);
			e.setHealth(health);
			
			//Adds the mobs UUID to the List so it doesn't keep rescaling
			mobUUID.add(e.getUniqueId().toString());
		}
	}
	
	public static int getDamage(Player p){
		int gearRating = PlayerUtils.getGearRating(p);
		int dmgMultiplier = PlayerUtils.dmgMultiplier(gearRating);
		
		return dmgMultiplier; 
	}
	
	public static boolean isBoss(LivingEntity e){
		String uuid = e.getUniqueId().toString();
		
		if(bossNames.contains(uuid))
			return true;
		else
			return false;
	}
	
}

