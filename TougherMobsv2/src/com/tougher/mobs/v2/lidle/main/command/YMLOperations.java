package com.tougher.mobs.v2.lidle.main.command;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class YMLOperations {

	//File path used for storing boss data
	public static final String FILE_PATH = "plugins//TougherMobs//Bosses//";

	//Set a value in the specified .yml file
	public static void ymlSet(String node, Object val, String bossName){
		File file = new File(FILE_PATH + bossName + ".yml");

		//If the file doesnt exist, make it exist
		if(!file.exists()){
			try{
				file.createNewFile();
				YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
				yml.set("name", bossName);
				yml.set(node, val);
				yml.save(file);
				return;
			} catch(IOException e){
				e.printStackTrace();
				return;
			}
		}

		try{
			YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
			yml.set(node, val);
			yml.save(file);
			return;
		} catch(IOException e){
			e.printStackTrace();
			return;
		}

	}
	
	//Set a list value in the specified .yml file *ONLY USES ITEMSTACKS*
	@SuppressWarnings("unchecked")
	public static void ymlSetList(String node, List<ItemStack> val, String bossName){
		File file = new File(FILE_PATH + bossName + ".yml");

		//If the file doesnt exist, make it exist
		if(!file.exists()){
			try{
				file.createNewFile();
				YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
				yml.set("name", bossName);
				
				//If the node contains data, copy it into the list
				if(yml.getList(node) != null)
					val.addAll((List<ItemStack>) yml.getList(node));
					
				yml.set(node, val);
				yml.save(file);
				return;
			} catch(IOException e){
				e.printStackTrace();
				return;
			}
		}
		
		//If the file does exist
		else{
			try{
				YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);

				//If the node contains data, copy it into the list
				if(yml.getList(node) != null)
					val.addAll((List<ItemStack>) yml.getList(node));

				yml.set(node, val);
				yml.save(file);
				return;
			} catch(IOException e){
				e.printStackTrace();
				return;
			}
		}
	}

	//Get a value from the specified .yml file
	public static Object ymlGet(String node, String bossName){
		File file = new File(FILE_PATH + bossName + ".yml");

		if(!file.exists())
			return null;
		
		YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
		return yml.get(node);

	}

//	public static int ymlGetInt(String node, String bossName){
//		
//	}
	
	//Get a list value from the specified .yml file *ONLY USES ITEMSTACKS*
	@SuppressWarnings("unchecked")
	public static List<ItemStack> ymlGetList(String node, String bossName){
		File file = new File(FILE_PATH + bossName+ ".yml");
		
		if(!file.exists())
			return null;
		
		YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
		
		return (List<ItemStack>) yml.getList(node);
	}

}
