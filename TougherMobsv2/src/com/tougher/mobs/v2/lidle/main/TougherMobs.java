package com.tougher.mobs.v2.lidle.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import com.tougher.mobs.v2.lidle.data.MobValues;
import com.tougher.mobs.v2.lidle.events.bosses.BossEvent;
import com.tougher.mobs.v2.lidle.events.custom_bosses.CustomBossEvent;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomBlaze;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomCaveSpider;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomCreeper;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomElderGuardian;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomEnderman;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomEndermite;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomEvoker;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomGhast;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomGuardian;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomHusk;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomMagmaCube;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomPigZombie;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomPolarBear;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomShulker;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomSilverfish;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomSkeleton;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomSlime;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomSpider;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomStray;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomVex;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomVindicator;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomWitch;
import com.tougher.mobs.v2.lidle.events.mobs.curr.CustomZombie;
import com.tougher.mobs.v2.lidle.events.mobs.old.BlazeEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.CaveSpiderEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.CreeperEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.ElderGuardianEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.EndermanEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.EndermiteEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.EvokerEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.GhastEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.GuardianEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.HuskEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.MagmaCubeEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.PigZombieEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.PolarBearEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.ShulkerEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.SilverfishEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.SkeletonEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.SlimeEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.SpiderEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.StrayEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.VexEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.VindicatorEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.WitchEvent;
import com.tougher.mobs.v2.lidle.events.mobs.old.ZombieEvent;
import com.tougher.mobs.v2.lidle.fun.LootBat;
import com.tougher.mobs.v2.lidle.main.command.BossCreatorCommand;
import com.tougher.mobs.v2.lidle.players.GlobalDamageEvents;

public class TougherMobs extends JavaPlugin{

	public static final String PLUGIN_NAME = "TougherMobs";
	public static final String PLUGIN_VERSION = " v2.2.2";
	public static final String PLUGIN_AUTHOR = " by Lidle";
	public static final String PLUGIN_MSG = PLUGIN_NAME + PLUGIN_VERSION + PLUGIN_AUTHOR;

	private static final String BOSS_CREATOR_PERMISSION = "toughermobs.createboss";

	//Global variables
	public static boolean mobArmor = true;
	public static Permission bossCreator = new Permission(BOSS_CREATOR_PERMISSION); //Permission to use the boss creator commands
	public static Random rand = new Random();
	public static TougherMobs pl;
		
	private static List<String> supported_worlds = new ArrayList<String>();

	public void onEnable(){
		init();	
		getLogger().info(PLUGIN_MSG + " enabled!");			
	}

	public void onDisable(){
		getLogger().info(PLUGIN_MSG + " disabled!");
		this.saveDefaultConfig();
	}

	private void init(){
		initConfig();
		initMetrics();
		pl = this;
		initEvents();
		initPermissions();
		initCommands();
		initSupportedMobsTxtFile();
	}

	private void initConfig(){
		this.getConfig().options().copyDefaults(true);
		this.saveDefaultConfig();
	}

	private void initPermissions(){
		this.getServer().getPluginManager().addPermission(bossCreator);
	}

	//Keeps track of plugin statistics
	private void initMetrics(){
		if (getConfig().getBoolean("enable-metrics")) {
			try {
				Metrics metrics = new Metrics(this);
				metrics.start();
			} catch (IOException e) {
				getLogger().info(e.getMessage());
			} 
		}
	}

	private void initEvents(){
		if(getConfig().getBoolean("blaze")){
			CustomBlaze.customMoves = this.getConfig().getBoolean("blaze-attacks");
			new CustomBlaze();
		}

		if(getConfig().getBoolean("bosses"))
			new BossEvent(this);

		if(getConfig().getBoolean("cave-spider")){
			new CustomCaveSpider();
		}

		if(getConfig().getBoolean("creeper")){
			CustomCreeper.blacklistedCreeperBlocks = this.getConfig().getIntegerList("blacklisted-creeper-blocks");
			CustomCreeper.customMoves = this.getConfig().getBoolean("creeper-fire-explosion");
			new CustomCreeper();
		}

		if(getConfig().getBoolean("elder-guardian")){
			new CustomElderGuardian();
		}

		if(getConfig().getBoolean("enderman")){
			CustomEnderman.blackListedEndermanBlocks = this.getConfig().getIntegerList("blacklisted-enderman-blocks");
			CustomEnderman.customMoves = this.getConfig().getBoolean("enderman-destroy");
			new CustomEnderman();
		}

		if(getConfig().getBoolean("endermite")){
			new CustomEndermite();
		}

		if(getConfig().getBoolean("evoker")){
			new CustomEvoker();
		}

		if(getConfig().getBoolean("ghast")){
			new CustomGhast();
		}

		if(getConfig().getBoolean("guardian")){
			new CustomGuardian();
		}

		if(getConfig().getBoolean("husk")){
			new CustomHusk();
		}

		if(getConfig().getBoolean("magma-cube")){
			new CustomMagmaCube();
		}

		if(getConfig().getBoolean("pig-zombie")){
			new CustomPigZombie();
		}

		if(getConfig().getBoolean("polar-bear")){
			PolarBearEvent.custom_moves = this.getConfig().getBoolean("polar-bear-attacks");
			new CustomPolarBear();
		}

		if(getConfig().getBoolean("shulker")){
			new CustomShulker();
		}

		if(getConfig().getBoolean("silverfish")){
			new CustomSilverfish();
		}

		if(getConfig().getBoolean("skeleton")){
			CustomSkeleton.customMoves = this.getConfig().getBoolean("skeleton-levitation");
			new CustomSkeleton();
		}

		if(getConfig().getBoolean("slime")){
			new CustomSlime();
		}

		if(getConfig().getBoolean("spider")){
			CustomSpider.customMoves = this.getConfig().getBoolean("spider-attacks");
			new CustomSpider();
		}

		if(getConfig().getBoolean("stray")){
			new CustomStray();
		}

		if(getConfig().getBoolean("vex")){
			new CustomVex();
		}

		if(getConfig().getBoolean("vindicator")){
			new CustomVindicator();
		}

		if(getConfig().getBoolean("witch")){
			new CustomWitch();
		}

		if(getConfig().getBoolean("zombie")){
			new CustomZombie();
		}

		if(getConfig().getBoolean("loot-bat")){
			new LootBat(this);
		}

		new GlobalDamageEvents(this);
		new CustomBossEvent(this);

		mobArmor = this.getConfig().getBoolean("mob-armor");
		supported_worlds = this.getConfig().getStringList("supported-worlds");
	}

	public void initCommands(){
		getCommand("createboss").setExecutor(new BossCreatorCommand());
	}

	public void initSupportedMobsTxtFile(){
		try{
			File file = new File("plugins//TougherMobs//Supported Mob Types.txt");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write("Supported Mobs for Bosses: ");
			bw.newLine();
			
			for(MobValues mv : MobValues.values()){
				bw.write(mv.toString());
				bw.newLine();
			}

			bw.close();
		} catch(IOException e){
			e.printStackTrace();
			return;
		}
	}

	public static boolean isSupportedWorld(String worldName){
		if(supported_worlds.contains(worldName))
			return true;
		return false;
	}

}
