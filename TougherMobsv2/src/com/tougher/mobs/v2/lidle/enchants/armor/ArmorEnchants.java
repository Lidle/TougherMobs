package com.tougher.mobs.v2.lidle.enchants.armor;

import org.bukkit.enchantments.Enchantment;

public enum ArmorEnchants{

	PROTECTION_ENVIRONMENTAL(Enchantment.PROTECTION_ENVIRONMENTAL, Enchantment.PROTECTION_ENVIRONMENTAL.getMaxLevel()),
	PROTECTION_EXPLOSIONS(Enchantment.PROTECTION_EXPLOSIONS, Enchantment.PROTECTION_EXPLOSIONS.getMaxLevel()),
	PROTECTION_FALL(Enchantment.PROTECTION_FALL, Enchantment.PROTECTION_FALL.getMaxLevel()),
	PROTECTION_FIRE(Enchantment.PROTECTION_FIRE, Enchantment.PROTECTION_FIRE.getMaxLevel()),
	PROTECTION_PROJECTILE(Enchantment.PROTECTION_PROJECTILE, Enchantment.PROTECTION_PROJECTILE.getMaxLevel()),
	DURABILITY(Enchantment.DURABILITY, Enchantment.DURABILITY.getMaxLevel()),
	FROST_WALKER(Enchantment.FROST_WALKER, Enchantment.FROST_WALKER.getMaxLevel()),
	OXYGEN(Enchantment.OXYGEN, Enchantment.OXYGEN.getMaxLevel()),
	THORNS(Enchantment.THORNS, Enchantment.THORNS.getMaxLevel()),
	WATER_WORKER(Enchantment.WATER_WORKER, Enchantment.WATER_WORKER.getMaxLevel());
	
	
	private Enchantment enchantment;
	private int maxLevel;
	
	public static final int SIZE = 10;
	
	ArmorEnchants(Enchantment enchantment, int maxLevel){
		this.enchantment = enchantment;
		this.maxLevel = maxLevel;
	}
	
	public Enchantment getEnchantment(){
		return enchantment;
	}
	
	public int getMaxLevel(){
		return maxLevel;
	}

}
